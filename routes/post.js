const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/auth");

const Post = require("../models/Post");

//@route GET api/posts
//@desc Read Post
//access Private
router.get("/", verifyToken, async (req, res) => {
  try {
    const posts = await Post.find({});
    res.json({ success: true, posts });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

//@route POST api/posts
//@desc Create Post
//access Private
router.post("/", verifyToken, async (req, res) => {
  const { image, title, description, url, status } = req.body;
  if (!title) {
    return res
      .status(400)
      .json({ success: false, message: "Title is require" });
  }
  try {
    const newPost = new Post({
      image,
      title,
      description,
      url: url.startsWith("https://") ? url : `https://${url}`,
      status: status,
    });
    await newPost.save();
    res.json({ success: true, message: "Happy Learning!", post: newPost });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

//@route PUT api/posts
//@desc Update Post
//access Private
router.put("/:id", verifyToken, async (req, res) => {
  const { image, title, description, url, status } = req.body;
  if (!title) {
    return res
      .status(400)
      .json({ success: false, message: "Title is require" });
  }
  try {
    let updatedPost = {
      image,
      title,
      description: description || "",
      url: url.startsWith("https://") ? url : `https://${url}` || "",
      status: status,
    };
    const postUpdateCondition = { _id: req.params.id };
    updatePost = await Post.findOneAndUpdate(postUpdateCondition, updatedPost, {
      new: true,
    });
    //User not authorised to update post or post not found
    if (updatedPost === null) {
      return res.status(401).json({
        success: false,
        message: "Post not found or user not authorised",
      });
    }
    res.json({
      success: true,
      message: "Post updated successfully",
      post: updatePost,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

//@route DELETE api/posts
//@desc Delete Post
//access Private
router.delete("/:id", verifyToken, async (req, res) => {
  try {
    const postDeleteCondition = { _id: req.params.id };
    const deletedPost = await Post.findOneAndDelete(postDeleteCondition);
    console.log(typeof deletedPost, `\n${deletedPost}`);
    //User not authorised to update post or post not found
    if (deletedPost === null) {
      return res.status(401).json({
        success: false,
        message: "Post not found or user not authorised",
      });
    }
    res.json({
      success: true,
      message: "Post deleted successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

module.exports = router;
